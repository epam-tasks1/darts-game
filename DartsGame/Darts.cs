﻿using System;

namespace DartsGame
{
    public static class Darts
    {
        public static int GetScore(double x, double y)
        {
            double hypotenuza = Math.Sqrt((x * x) + (y * y));

            if (hypotenuza <= 1)
            {
                return 10;
            }

            if (hypotenuza <= 5)
            {
                return 5;
            }

            if (hypotenuza <= 10)
            {
                return 1;
            }

            return 0;
        }
    }
}
